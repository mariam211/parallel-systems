# Advanced parallel systems: Reduce-Scatter

## Description

Implementation of the reduce scatter algorithm using MPI.


## Project Structure

In the root directory, you will find:

- **src**: Contains source code
- **exec**: Contains executables
- **plot**: contains code to plot and the results


## Miscellaneous commands

Run a program:

```bash
mpirun -np <Number of processes> ./exec/<program name>
```
