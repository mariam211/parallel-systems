#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include <stdlib.h>
#include <math.h>

int receiving_rank(int rank, int level)
{
    int mod = pow(2, level);
    int distance = mod/2;
    int recv_rank = (rank+distance)%mod + ((int)rank/mod)*mod;

    return recv_rank;
}


int main(int argc, char *argv[])
{
    int my_rank, comm_size;
    int received_num;
    MPI_Win win;
    MPI_Init(&argc, &argv);
    MPI_Status status;
    MPI_Datatype l_type;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size( MPI_COMM_WORLD, &comm_size );
    int* numbers = malloc(comm_size*sizeof(int)); 

    for(int i=0; i<comm_size; i++){
        numbers[i] = i + my_rank*10;
    }

      int levels, level;

    levels = (int)(log((double)comm_size)/log(2.0));

    if(comm_size != pow(2,levels)){
        if(my_rank==0){
            printf("Number of processes should be a power of two");
        }
        return 1;
    }

    int ack, recv_ack=comm_size;
    int count=comm_size/2;
    int start_rank=0;
    
    MPI_Win_create(numbers, comm_size*sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
    MPI_Win_fence(0, win);

    for(int level=1, ack=1, stride=2, count=comm_size/2, start_rank=0, block_len=1; level<=levels  ; level++, stride*=2, count/=2){
        int target_rank = receiving_rank(my_rank, level);
        start_rank = target_rank%stride ; //get_start_rank(target_rank, count, stride, comm_size);
        int my_start_rank = my_rank%stride ; //get_start_rank(my_rank, count, stride, comm_size);
        //printf("rank %d target %d start %d my start %d\n", my_rank, target_rank, start_rank, my_start_rank);

        // if(my_rank==0){
        //     printf("count %d stride %d\n", count, stride);
        // }
        //count, blck len, stride
        MPI_Type_vector(count, block_len, stride, MPI_INT, &l_type);
        MPI_Type_commit(&l_type);


        MPI_Accumulate(&numbers[target_rank], 1, l_type, target_rank, target_rank, 1, l_type, MPI_SUM, win);
    }
    
    MPI_Win_fence(0, win);

         printf("rank %d got:\n", my_rank);
        for(int i=0; i<comm_size; i++){
            printf("%d\t", numbers[i]);
        }
        printf("\n");

    if(my_rank==0){
        int* results = malloc(comm_size*sizeof(int)); 
        for(int i=0; i<comm_size; i++){
            results[i] = 0;
        }
        printf("expected result\n");

        for(int k=0; k<comm_size; k++){
            for(int i=0; i<comm_size; i++){
                results[i] += i + k*10;
            }
        }

        for(int i=0; i<comm_size; i++){
            printf("%d\t", results[i]);
        }
        printf("\n");
        free(results);
    }

    free(numbers);
    //free(received_numbers);
    MPI_Win_free(&win);
    //MPI_Type_free(&l_type);
    MPI_Finalize();
}
