#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


int main(int argc, char *argv[])
{
    int my_rank, comm_size;
    MPI_Win win;
    MPI_Init(&argc, &argv);
    MPI_Status status;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size( MPI_COMM_WORLD, &comm_size );
    int* numbers = malloc(comm_size*sizeof(int)); 

    //initialize array
    for(int i=0; i<comm_size; i++){
        numbers[i] = i + my_rank*10;
    }

    MPI_Win_create(numbers, comm_size*sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &win);
    
    //start timer
    clock_t start;
    if(my_rank==0)
        start = clock();
    
    //start synchronization
    MPI_Win_fence(0, win);
    for(int i=1; i<comm_size;i++){
        int target_rank = (my_rank+i)%comm_size;
        MPI_Accumulate(&numbers[target_rank], 1, MPI_INT, target_rank, target_rank, 1, MPI_INT, MPI_SUM, win);
    }
    MPI_Win_fence(0, win);

    //Duration
    int duration;
    if(my_rank==0){
        clock_t end = clock();
        duration = (end - start);
    }

    //Show result
    printf("rank %d computed %d:\n", my_rank, numbers[my_rank]);
    for(int i=0; i<comm_size; i++){
        printf("%d\t", numbers[i]);
    }
    printf("\n");

    //Compute the reduced result
    if(my_rank==0){
        int* results = malloc(comm_size*sizeof(int)); 
        for(int i=0; i<comm_size; i++){
            results[i] = 0;
        }
        printf("expected reduce result\n");

        for(int k=0; k<comm_size; k++){
            for(int i=0; i<comm_size; i++){
                results[i] += i + k*10;
            }
        }

        for(int i=0; i<comm_size; i++){
            printf("%d\t", results[i]);
        }
        printf("\n");
        free(results);
    }

    if(my_rank==0){
        printf("Duration %d\n", duration);
    }

    free(numbers);
    MPI_Win_free(&win);
    MPI_Finalize();
}
