#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


int receiving_rank(int rank, int stride)
{
    //stride = pow(2, level)
    //distance between two processes
    int distance = stride/2;
    int recv_rank = (rank+distance)%stride + ((int)(rank/stride))*stride;

    return recv_rank;
}


int main(int argc, char *argv[])
{
    int my_rank, comm_size, levels, level;
    MPI_Init(&argc, &argv);
    MPI_Status status;
    MPI_Datatype l_type;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size( MPI_COMM_WORLD, &comm_size );
    int* numbers = malloc(comm_size*sizeof(int)); 
    int* received_numbers = malloc(comm_size*sizeof(int)); 

    //initialize array
    for(int i=0; i<comm_size; i++){
        numbers[i] = i + my_rank*10;
    }

    for(int i=0; i<comm_size; i++){
        received_numbers[i] = 0;
    }

    //compute levels needed
    levels = (int)(log((double)comm_size)/log(2.0));

    if(comm_size != pow(2,levels)){
        if(my_rank==0){
            printf("Number of processes should be a power of two");
        }
        return 1;
    }

    int count=comm_size/2;
    int start_rank=0;

    //start timer
    clock_t start;
    if(my_rank==0)
        start = clock();

    //start communication
    for(int level=1, stride=2, count=comm_size/2, start_rank=0, block_len=1; level<=levels; level++, stride*=2, count/=2){
        int target_rank = receiving_rank(my_rank, stride);
        start_rank = target_rank%stride ;
        int my_start_rank = my_rank%stride ; 

        //use vector to be able to send different data sizes and parts of the local array
        MPI_Type_vector(count, block_len, stride, MPI_INT, &l_type);
        MPI_Type_commit(&l_type);
        MPI_Request requests[2];

        MPI_Irecv(&received_numbers[my_start_rank], 1, l_type, target_rank, 1, MPI_COMM_WORLD, &requests[0]);
        MPI_Isend(&numbers[start_rank], 1, l_type, target_rank, 1, MPI_COMM_WORLD, &requests[1]);

        MPI_Waitall(2, requests, MPI_STATUS_IGNORE);

        //reduce received numbers
        for(int i=my_start_rank; i<comm_size; i++){
            numbers[i] += received_numbers[i];
            received_numbers[i] = 0;
        }

        //barrier since all processes should have completed their reduction before going to next step
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Type_free(&l_type);
    }

    //start timer
    int duration;
    if(my_rank==0){
        clock_t end = clock();
        duration = (end - start);
    }
    
    //show results
    printf("rank %d computed %d:\n", my_rank, numbers[my_rank]);
    for(int i=0; i<comm_size; i++){
        printf("%d\t", numbers[i]);
    }
    printf("\n");

    //show expected reduction to compare
    if(my_rank==0){
        int* results = malloc(comm_size*sizeof(int)); 
        for(int i=0; i<comm_size; i++){
            results[i] = 0;
        }
        printf("expected reduced result\n");

        for(int k=0; k<comm_size; k++){
            for(int i=0; i<comm_size; i++){
                results[i] += i + k*10;
            }
        }

        for(int i=0; i<comm_size; i++){
            printf("%d\t", results[i]);
        }
        printf("\n");
        free(results);
    }
    
    if(my_rank==0){
        printf("Duration %d\n", duration);
    }

    free(numbers);
    free(received_numbers);
    MPI_Finalize();
}
