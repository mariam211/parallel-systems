#include <stdio.h>
#include <string.h>
#include <mpi.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


int main(int argc, char *argv[])
{
    int my_rank, comm_size;
    MPI_Init(&argc, &argv);
    MPI_Status status;
    MPI_Datatype l_type;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	MPI_Comm_size( MPI_COMM_WORLD, &comm_size );
    int* numbers = malloc(comm_size*sizeof(int)); 

    //initialize array
    for(int i=0; i<comm_size; i++){
        numbers[i] = i + my_rank*10;
    }

    //start timer
    clock_t start;
    if(my_rank==0)
        start = clock();

    //start communication
    for(int i=1; i<comm_size;i++){
        int target_rank = (my_rank+i)%comm_size;
        int origin_rank = (my_rank-i+comm_size)%comm_size;
        int recv_number = 0;
        MPI_Request requests[2];

        MPI_Irecv(&recv_number, 1, MPI_INT, origin_rank, 1, MPI_COMM_WORLD, &requests[0]);
        MPI_Isend(&numbers[target_rank], 1, MPI_INT, target_rank, 1, MPI_COMM_WORLD, &requests[1]);

        MPI_Waitall(2, requests, MPI_STATUS_IGNORE);

        //not necessary to do here we could just store all received values and do reduction at the end
        numbers[my_rank] += recv_number;
    }
    
    //compute exec duration
    int duration;
    if(my_rank==0){
        clock_t end = clock();
        duration = (end - start);
    }

    //show results
    printf("rank %d computed %d:\n", my_rank, numbers[my_rank]);
    for(int i=0; i<comm_size; i++){
        printf("%d\t", numbers[i]);
    }
    printf("\n");

    //show the reduced array
    if(my_rank==0){
        int* results = malloc(comm_size*sizeof(int)); 
        for(int i=0; i<comm_size; i++){
            results[i] = 0;
        }
        printf("expected result\n");

        for(int k=0; k<comm_size; k++){
            for(int i=0; i<comm_size; i++){
                results[i] += i + k*10;
            }
        }

        for(int i=0; i<comm_size; i++){
            printf("%d\t", results[i]);
        }
        printf("\n");
        free(results);
    }

    if(my_rank==0){
        printf("Duration %d\n", duration);
    }
    
    free(numbers);
    MPI_Finalize();
}
